<!--
SPDX-FileCopyrightText: (C) 2024 Federica Bazzocchi <federica.bazzocchi@areasciencepark.it>
SPDX-FileCopyrightText: (C) 2024 Marco Prenassi <marco.prenassi@areasciencepark.it>
SPDX-FileCopyrightText: (C) 2024 Ruggero Lot <ruggero.lot@areasciencepark.it>
SPDX-FileCopyrightText: (C) 2024 Tommaso Rodani <tommaso.rodani@areasciencepark.it>

SPDX-License-Identifier: CC-BY-4.0
-->

Start playing around with [openAPI](https://www.openapis.org/) to define the API required for the OFED platform.

[Look at the api](./src/openapi.yaml)

## Resources and thanks to:

- [OpenAPI specification](https://swagger.io/specification/)
- [openAPI guide official](https://learn.openapis.org/)
- [openAPI guide by swagger](https://swagger.io/docs/specification/about/)
- [petstore Example](https://petstore.swagger.io/) Navigation
- [petstore Example](https://github.com/swagger-api/swagger-petstore/blob/master/src/main/resources/openapi.yaml) Yaml
- [opinionated boilerplate](https://github.com/dgarcia360/openapi-boilerplate)
